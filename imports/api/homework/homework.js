import { Mongo } from 'meteor/mongo';

const Homework = new Mongo.Collection('homework');

export default Homework;
