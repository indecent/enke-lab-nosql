import Homework from './homework';


export default {  
    Mutation: {
      addHomework(obj, { name, due, description, courseId }, { userId }) {
        if(userId) {
          const homeworkId = Homework.insert({
            name,
            due,
            description,
            courseId
          });
          return Homework.findOne(homeworkId);
        }
        throw new Error('Unauthorized');
      }
    }
  
  
  }
  