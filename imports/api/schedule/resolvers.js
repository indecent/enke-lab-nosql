import Schedule from './schedule';


export default {  
    Mutation: {
      addSchedule(obj, { startTimes, endTimes, courseId }, { userId }) {
        if(userId) {
          const scheduleId = Schedule.insert({
            startTimes,
            endTimes,
            courseId
          });
          return Schedule.findOne(courseId);
        }
        throw new Error('Unauthorized');
      }
    }
  
  
  }
  