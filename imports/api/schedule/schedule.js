import { Mongo } from 'meteor/mongo';

const Schedule = new Mongo.Collection('schedules');

export default Schedule;
