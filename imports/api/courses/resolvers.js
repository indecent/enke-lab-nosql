import Courses from './courses';
import Students from '../students/student';
import Comments from '../comments/comments';
import Schedule from '../schedule/schedule';
import Homework from '../homework/homework';

export default {
  Query: {
    facultyCourses(obj, { professorId }, context) {
      return Courses.find({
        professorId
      }).fetch();
    },

    studentCourses(obj, { studentId }, context) {
      return Courses.find({
        students: studentId
      }).fetch();
    },

    allCourses(obj, args, context) {
      return Courses.find({
        
      }).fetch()
    },

    getCourse(obj, { _id }, context) {
      return Courses.find({
        _id
      }).fetch();
    }
  },

  Course: {
    students: (course) =>
      Students.find({
        courseId: course._id
      }).fetch(),
    
    comments: (course) =>
      Comments.find({
        courseId: course._id
      }).fetch(),

    homework: (course) =>
      Homework.find({
        courseId: course._id
      }).fetch(),

    schedule: (course) =>
      Schedule.find({
        courseId: course._id
      }).fetch()
  },

  Mutation: {
    createCourse(obj, { name, description, professorId }, { userId }) {
      if(userId) {
        const courseId = Courses.insert({
          name,
          description,
          students: [],
          professorId,
          comments: [],
          schedule: [],
          homework: []
        });
        return Courses.findOne(courseId);
      }
      throw new Error('Unauthorized');
    }
  }


}
