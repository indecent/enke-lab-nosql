import { Mongo } from 'meteor/mongo';

const Courses = new Mongo.Collection('courses');

export default Courses;
