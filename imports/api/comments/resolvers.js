import Comments from './comments';

export default {
    Mutation: {
        addComment(obj, { author, comment, courseId }, { userId }) {
            if(userId) {
              const commentId = Comments.insert({
                author,
                comment,
                courseId
              });
              return Comments.findOne(commentId)
            }
          }
    }
}