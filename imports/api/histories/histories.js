import { Mongo } from 'meteor/mongo';

const Histories = new Mongo.Collection('histories');

export default Histories;
