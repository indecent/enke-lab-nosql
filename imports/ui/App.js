import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { withApollo } from 'react-apollo';
import Main from './main/Main';

const App = ({ loading, client, user}) => {
  if(loading) return null;
    return (
      <div>
        <Main user={user} client={client} loading={loading}/>
      </div>
    )
};

const userQuery = gql`
  query User {
    user {
      _id
      profile {
        firstName
        lastName
        isFaculty
      }
    }
  }
`;

export default graphql(userQuery, {
  props: ({ data }) => ({ ...data })
})(withApollo(App));
