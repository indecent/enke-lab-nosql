import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { Accounts } from 'meteor/accounts-base';
import { Modal, Col, Card, Input, Row } from 'react-materialize';

const addUserInfo = gql `
  mutation addUserInfo($fName: String!, $lName: String!) {
    addUserInfo(fName: $fName, lName: $lName) {
      _id
    }
  }
`;

class RegisterForm extends Component {
  registerUser = (e) => {
    e.preventDefault();
    Accounts.createUser({
      email: this.email.input.value,
      password: this.password.input.value,
      profile: {
        lastName: this.lastName.input.value,
        firstName: this.firstName.input.value,
        isFaculty: this.isFaculty.input.checked
      }
    }, (error) => {
      if(!error) {
        this.props.client.resetStore();
      }
      console.log('Something happened...');
      console.log(error);
    });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.registerUser}>
          <Row>
            <Col s={12}>
              <Row>
                <Col s={12}>
                  <Col s={3}/>
                  <Input s={6} id="email" type="email" label="Email" ref={input => (this.email = input)}/>
                </Col>
                <Col s={12}>
                  <Col s={3}/>
                  <Input s={6} id="password" type="password" label="Password" ref={input => (this.password = input)}/>
                </Col>
                <Col s={12}>
                  <Col s={3}/>
                  <Input s={6} id="fname" type="text" label="First Name" ref={input => (this.firstName = input)}/>
                </Col>
                <Col s={12}>
                  <Col s={3}/>
                  <Input s={6} id="lname" type="text" label="Last Name" ref={input => (this.lastName = input)}/>
                </Col>
                <Col s={12}>
                  <Col s={3}/>
                  <Input s={6} id="isFaculty" type="checkbox" label="Faculty" ref={input => (this.isFaculty = input)}/>
                </Col>
                <br/>
                <Row>
                  <div className="col m2 s2 offset-m8 offset-s6">
                    <button className="blue darken-4 btn" type="submit">Register</button>
                  </div>
                </Row>
              </Row>
            </Col>
          </Row>
        </form>
      </div>
    );
  }
}

export default graphql(addUserInfo, {
  name: 'addUserInfo',
  options: {
    refetchQueries: [
      'Users'
    ]
  }
})(RegisterForm)
