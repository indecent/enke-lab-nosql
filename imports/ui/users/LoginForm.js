import React, { Component } from 'react';
import { Modal, Col, Card, Input, Row } from 'react-materialize';

export default class LoginForm extends Component {
  login = (e) => {
    e.preventDefault();
    Meteor.loginWithPassword(this.email.value, this.password.value,
        error => {
      if(!error) {
        this.props.client.resetStore();
      }
    });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.login}>
        <Row>
            <Col s={12}>
              <Row>
                <Col s={12}>
                  <Col s={3}/>
                  <Input s={6} id="email" type="email" label="Email" ref={input => (this.email = input)}/>
                </Col>
                <Col s={12}>
                  <Col s={3}/>
                  <Input s={6} id="password" type="password" label="Password" ref={input => (this.password = input)}/>
                </Col>
                <br/>
                <Row>
                  <div className="col m2 s2 offset-m8 offset-s6">
                    <button className="blue darken-4 btn" type="submit">Login</button>
                  </div>
                </Row>
              </Row>
            </Col>
          </Row>
        </form>
      </div>
    );
  }
}
