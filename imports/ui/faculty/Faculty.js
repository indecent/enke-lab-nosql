import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Home from './Home';
import Course from './../courses/Course';
import NewCourse from '../courses/NewCourseForm';
import CourseSearch from '../courses/CourseSearch';
import Schedule from '../courses/Schedule';
import Homework from '../courses/Homework';

export default class Faculty extends Component {
    render() {
        const { user, client } = this.props;
        return (
            <div>
                <Route exact path="/" render={(obj) => <Home user={user} client={client} obj={obj}/>}/>
                <Route path="/course/:id" render={(obj) => <Course user={user} client={client} obj={obj}/>}/>
                <Route path="/newcourse" render={(obj) => <NewCourse user={user} client={client} obj={obj}/>}/>
                <Route path="/coursesearch" render={(obj) => <CourseSearch user={user} client={client} obj={obj}/>}/>
                <Route path="/schedule/:id" render={(obj) => <Schedule user={user} client={client} obj={obj}/>}/>
                <Route path="/homework/:id" render={(obj) => <Homework user={user} client={client} obj={obj}/>}/>
            </div>
        )
    }
}