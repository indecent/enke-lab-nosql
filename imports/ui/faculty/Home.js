import React, { Component } from 'react';
import CourseList from '../courses/CourseList';

export default class Home extends Component {
    render() {
        const { user, client } = this.props;
        return (
            <div>
                <CourseList user={user} client={client}/>
            </div>
        )
    }
}