import React, { Component } from 'react';
import RegisterForm from '../users/RegisterForm';
import LoginForm from '../users/LoginForm';
import Header from '../header/Header'
import Faculty from '../faculty/Faculty';
import Student from '../students/Student';
import Welcome from './Welcome';
import { BrowserRouter, BrowserHistory } from 'react-router-dom';



export default class Main extends Component {
  state = {
    login: true
  };

  setLogin = () => {
    this.setState({
      login: !this.state.login
    });
  }

  
  render() {
  
    const { client, user, loading } = this.props;
    const { login } = this.state;

    if(loading) return null;
    if(user._id) {
      if(user.profile.isFaculty) {
        return (
          <div>
            <BrowserRouter history={BrowserHistory}>
              <div>
                <Header user={user} client={client} login={this.state.login}/>
                <Faculty user={user} client={client} login={this.state.login}/>
              </div>
            </BrowserRouter>
          </div>
        );
      } else {
        return (
          <div>
            <BrowserRouter history={BrowserHistory}>
              <div>
                <Header user={user} client={client} login={this.state.login}/>
                <Student user={user} client={client} login={this.state.login}/>
              </div>
            </BrowserRouter>
          </div>
        );
      }
    }
    return (
      <div>
        <BrowserRouter history={BrowserHistory}>
        {login ? (
            <div>
              <Header user={user} client={client} login={this.state.login} setLogin={this.setLogin.bind(this)}/>
              <Welcome/>
              <LoginForm client={client} />
            </div>
        ) : (
            <div>
              <Header user={user} client={client} login={this.state.login} setLogin={this.setLogin.bind(this)}/>
              <Welcome/>
              <RegisterForm client={client} />
            </div>
        )}
        </BrowserRouter>
    </div>
    )
  }
};
