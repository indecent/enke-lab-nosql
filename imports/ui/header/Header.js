import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Header extends Component {
            
    render() {
        
        const { user, client, login, setLogin } = this.props;
        if (user._id) {
            if (user.profile.isFaculty) {
                return (
                    <div>
                        <div className="row blue darken-4">
                            <div className="col s12">
                                <div className="col">
                                    <img src='/images/ast.png' alt="Faculty and Student Team" height="55" width="55"></img>
                                </div>
                                <div className="col right">
                                    <Link to="/" className="amber accent-3 btn header-btn" onClick={() => {
                                        Meteor.logout();
                                        client.resetStore();
                                    }}>
                                        Logout
                                    </Link>
                                </div>
                                <div className="col right">  
                                    <ul className="col">
                                        <li className="col">
                                            <Link to="/">Home</Link>
                                        </li>
                                        <li className="col">
                                            <Link to="/newcourse">New Course</Link>
                                        </li>
                                        <li className="col">
                                            <Link to="/coursesearch">Course Search</Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
            return (
                <div>
                    <div className="row blue darken-4">
                        <div className="col s12">
                            <div className="col">
                                <img src="/images/ast.png" alt="Faculty and Student Team" height="55" width="55"></img>
                            </div>
                            <div className="col right">
                                <Link to="/" className="amber accent-3 btn header-btn" onClick={() => {
                                    Meteor.logout();
                                    client.resetStore();
                                }}>
                                    Logout
                                </Link>
                            </div>
                            <div className="col right">  
                                <ul className="col">
                                    <li className="col">
                                        <Link to="/">Home</Link>
                                    </li>
                                    <li className="col">
                                        <Link to="/coursesearch">Course Search</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
        <div>
            <div className="row blue darken-4">
                <div className="col s12">
                    <div className="col">
                        <img src='/images/ast.png' alt="Faculty and Student Team" height="55" width="55"></img>
                    </div>
                    <div className="right-align">
                        <a className="amber accent-3 btn header-btn" onClick={() => setLogin()}>
                            {login ? 'Register' : 'Login'}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}