import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { Link } from 'react-router-dom';

const createCourse = gql`
    mutation createCourse($name: String!, $description: String!, $professorId: String!) {
        createCourse(name: $name, description: $description, professorId: $professorId) {
            _id
        }
    }
`;

class NewCourseForm extends Component {
    state = {
        error: null
    }

    submitForm = () => {
        this.props.createCourse({
            variables: {
                name: this.name.value,
                description: this.desc.value,
                professorId: this.professorId.value
            }
        })
        .then(() => {
            this.name.value = '',
            this.desc.value = ''
        })
        .catch(error => {
            this.setState({error: error.message});
        });
    };

    render() {
        const { user } = this.props;
        if (user.profile.isFaculty) {
            return (
                <div>
                    {this.state.error && <p>{this.state.error}</p>}
                    <h3 className="center">Add a New Course</h3>
                    <form>
                        <div className="row">
                            <div className="input-field col s6 offset-s3">
                                <input id="courseName" type="text" ref={input => (this.name = input)}/>
                                <label htmlFor="courseName">Name</label>
                            </div>
                            <div className="input-field col s6 offset-s3">
                                <textarea id="courseDesc" className="materialize-textarea" type="text" ref={input => (this.desc = input)}/>
                                <label htmlFor="courseDesc">Description</label>
                            </div>
                            <input type="hidden" value={user._id} ref={input => (this.professorId = input)}/>
                        </div>
                        <div className="row">
                            <div className="col m2 s2 offset-m8 offset-s6">
                                <Link to="/" className="btn" onClick={this.submitForm}>Submit</Link>
                            </div>
                        </div>
                    </form>
                </div>
            );
        } else {
            return (
                <h2>You do not have permission to view this page</h2>
            )
        }
    }
}

export default graphql(createCourse, {
    name: 'createCourse',
    options: {
        refetchQueries: [
            'courses'
        ]
    }
})(NewCourseForm)