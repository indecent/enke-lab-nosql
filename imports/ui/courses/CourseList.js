import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';

const facultyCourses = gql`
    query facultyCourses($professorId: String!) {
        facultyCourses(professorId: $professorId) {
            _id
            name
            description
            students {
                _id
            }
        }
    }
`;

const allCourses = gql`
    query allCourses {
        allCourses {
            _id
            name
            students {
                uid
                fname
                lname
            }
        }
    }
`;

export default class CourseList extends Component {
    render() {
        const { user, client } = this.props;
        if (user.profile.isFaculty) {
            return (
                <Query query={facultyCourses} variables={{professorId: user._id}} pollInterval={1000}>
                {({ data, loading, error, startPolling, stopPolling }) => {
                    if (loading) {
                        return (
                            <div>
                                Loading ...
                            </div>
                        )
                    }

                    if (error) {
                        console.log(error);
                        return (
                            <div>
                                An unexpected error occured.<br/>
                            </div>
                        )
                    }

                    const courses = data.facultyCourses
                    return (
                        <div>
                            <div className="row">
                                <h3 className="center">Your courses</h3>
                            </div>
                            <div className="row">
                                <div className="col s10 offset-s1">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Course</th>
                                                <th>Description</th>
                                                <th>Enrolled</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {courses.map(course => (
                                                <tr key={course._id} className="row">
                                                    <td>
                                                    <Link to={"/course/" + course._id} >{course.name}</Link>
                                                    </td>
                                                    <td className="truncate">{course.description}</td>
                                                    <td>{course.students.length}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    
                                </div>
                            </div>
                        </div>
                    )
                }}
                </Query>
            )
        } else {
            return (
                <Query query={allCourses} pollInterval={500}>
                {({ data, loading, error, startPolling, stopPolling }) => {
                    if (loading) {
                        return (
                            <div>
                                Loading ...
                            </div>
                        )
                    }

                    if (error) {
                        console.log(error);
                        return (
                            <div>
                                An unexpected error occured.<br/>
                            </div>
                        )
                    }

                    const courses = data.allCourses
                    console.log(courses)
                    var enrolledCourses = []
                    for (var i = 0; i < courses.length; i++) {
                        for (var j = 0; j < courses[i].students.length; j++) {
                            console.log(courses[i].students[j])
                            if(courses[i].students[j].uid == user._id) {
                                enrolledCourses.push(courses[i])
                            }
                        }
                    }
                    return (
                        <div>
                            <div className="row">
                                <h3 className="center">Your courses</h3>
                            </div>
                            <div className="row">
                                <div className="col s10 offset-s1">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Course</th>
                                                <th>Description</th>
                                                <th>Enrolled</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {enrolledCourses.map(course => (
                                                <tr key={course._id} className="row">
                                                    <td>
                                                    <Link to={"/course/" + course._id} >{course.name}</Link>
                                                    </td>
                                                    <td className="truncate">{course.description}</td>
                                                    <td>{course.students.length}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    
                                </div>
                            </div>
                        </div>
                    )
                }}
                </Query>
            )
        }
    }
}