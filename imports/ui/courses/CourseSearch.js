import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';

const getCourses = gql`
    query allCourses {
        allCourses {
            _id
            name
            description
            students {
                _id
            }
        }
    }
`

export default class CourseList extends Component {
    constructor() {
        super();

        this.handleSearch = this.searchKeyUp.bind(this);
    }

    searchKeyUp(e) {
        var searchElements = document.getElementById('tbody').getElementsByTagName('tr');

        for (var i = 0; i < searchElements.length; i++) {
            if (!searchElements[i].firstChild.innerText.toLowerCase().includes(e.target.value.toLowerCase())) {
                searchElements[i].style.display="none"
            } else {
                searchElements[i].style.display="table-row"
            }
        }
    }

    render() {
        const { user, client } = this.props;
        return (
            <Query query={getCourses} pollInterval={1000}>
            {({ data, loading, error, startPolling, stopPolling }) => {
                if (loading) {
                    return (
                        <div>
                            Loading ...
                        </div>
                    )
                }

                if (error) {
                    console.log(error);
                    return (
                        <div>
                            An unexpected error occured.<br/>
                        </div>
                    )
                }

                const courses = data.allCourses
                return (
                    <div>
                        <div className="row">
                            <div className="col s12">
                            <div className="input-field col s6 offset-s3">
                                <input id="courseSearch" type="text" onKeyUp={this.handleSearch} ref={input => (this.courseSearch = input)}/>
                                <label htmlFor="courseSearch">Search</label>
                            </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col s10 offset-s1">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Course</th>
                                            <th>Description</th>
                                            <th>Enrolled</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                        {courses.map(course => (
                                            <tr key={course._id} className={"row"}>
                                                <td>
                                                <Link to={"/course/" + course._id} >{course.name}</Link>
                                                </td>
                                                <td className="truncate">{course.description}</td>
                                                <td>{course.students.length}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                    
                            </div>
                        </div>
                    </div>
                )
            }}
            </Query>
        )
    }
}