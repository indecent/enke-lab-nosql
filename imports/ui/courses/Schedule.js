import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { Modal, Col, Card, Input, Row } from 'react-materialize';
import { Link } from 'react-router-dom';

const getCourse = gql`
    query getCourse($id: String!) {
        getCourse(_id: $id) {
            _id
            name
            schedule {
                _id
                startTimes
                endTimes
            }
        }
    }
`

const addSchedule = gql`
    mutation addSchedule($days: [String]!, $startTime: String!, $endTime: String!, $courseId: String!) {
        addSchedule(days: $days, startTime: $startTime, endTime: $endTime, courseId: $courseId) {
            _id
        }
    }
`

const newSchedule = () => {
    let input;
}

export default class Schedule extends Component {

    render() {
        const { user, client, obj } = this.props;
        const courseId = obj.match.params.id

        return (
            <div>
                <Query query={getCourse} variables={{id: courseId}} pollInterval={1000}>
                {({ data, loading, error, startPolling, stopPolling }) => {
                    if (loading) {
                        return (
                            <div>
                                Loading ...
                            </div>
                        )
                    }

                    if (error) {
                        console.log(error);
                        return (
                            <div>
                                An unexpected error occured.<br/>
                            </div>
                        )
                    }

                    const course = data.getCourse[0]
                    const schedule = course.schedule
                    if (user.profile.isFaculty) {
                        return (
                            <div>
                            <div className="row">
                                <div className="col s10 offset-s1">
                                    <div className="col">
                                        <Link to={"/course/:id" + course._id}>Back</Link>
                                    </div>
                                </div>
                            </div>
                                <div className="row">
                                    <div className="col s10 offset-s1">
                                        <h4 className="">{course.name}</h4>
                                        <br/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s10 offset-s1">
                                        <h4>Schedule</h4>
                                    </div>
                                    <div className="col s10 offset-s1">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>Monday</th>
                                                    <th>Tuesday</th>
                                                    <th>Wednesday</th>
                                                    <th>Thursday</th>
                                                    <th>Friday</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {schedule.map(sched => (
                                                    <tr key={sched._id} className="row">
                                                        <td>{sched.startTime} - {sched.endTime}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br/><br/><br/><br/>
                            <Mutation mutation={addSchedule}>
                            {(newSchedule, { data }) => {
                                return (
                                    <div>
                                        <div className="row">
                                            <div className="col s10 offset-s1">
                                                <p>Add days and times for this course.</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <form id='scheduleForm' onSubmit={e => {
                                                    e.preventDefault();
                                                    var days = [],
                                                        timesArr = [],
                                                        form = document.getElementById('scheduleForm'),
                                                        times = form.childNodes[0]

                                                    for (var i = 0; i < times.childNodes.length; i++) {
                                                        console.log(times.childNodes[i].childNodes)
                                                        for (var j = 0; j < times.childNodes[i].childNodes.length; i++) {
                                                            if (times.childNodes[i].childNodes[0].name === 'monStartTime' && times.childNodes[1].name === 'monEndTime') {
                                                                timesArr.push(times.childNodes[i].childNodes[0].value)
                                                            }
                                                            if (times.childNodes[i].childNodes[0].name === 'tueStartTime' && times.childNodes[1].name === 'tueEndTime') {
                                                                timesArr.push(times.childNodes[i].childNodes[0].value)
                                                            }
                                                            if (times.childNodes[i].childNodes[0].name === 'wedStartTime' && times.childNodes[1].name === 'wedEndTime') {
                                                                timesArr.push(times.childNodes[i].childNodes[0].value)
                                                            }
                                                            if (times.childNodes[i].childNodes[0].name === 'thuStartTime' && times.childNodes[1].name === 'thuEndTime') {
                                                                timesArr.push(times.childNodes[i].childNodes[0].value)
                                                            }
                                                            if (times.childNodes[i].childNodes[0].name === 'friStartTime' && times.childNodes[1].name === 'friEndTime') {
                                                                timesArr.push(times.childNodes[i].childNodes[0].value)
                                                            }
                                                        }
                                                    }
                                                    console.log(timesArr)

                                                    newSchedule({ variables: {days: days, startTime: timesArr[0], endTime: timesArr[1], courseId: course._id}})
                                                }}>
                                                    <div className="row">
                                                        <div className="col s10 offset-s1">
                                                            <Input label='Start' name='monStartTime' type='time' onChange={function(e, value) {}} ref={input => (this.monStartTime = input)}/>
                                                            <Input label='Start' name='tueStartTime' type='time' onChange={function(e, value) {}} ref={input => (this.tueStartTime = input)}/>
                                                            <Input label='Start' name='wedStartTime' type='time' onChange={function(e, value) {}} ref={input => (this.wedStartTime = input)}/>
                                                            <Input label='Start' name='thuStartTime' type='time' onChange={function(e, value) {}} ref={input => (this.thuStartTime = input)}/>
                                                            <Input label='Start' name='friStartTime' type='time' onChange={function(e, value) {}} ref={input => (this.friStartTime = input)}/>
                                                        </div>
                                                        <div className="col s10 offset-s1">
                                                            <Input label='Finish' name='monEndTime' type='time' onChange={function(e, value) {}} ref={input => (this.monEndTime = input)}/>
                                                            <Input label='Finish' name='tueEndTime' type='time' onChange={function(e, value) {}} ref={input => (this.tueEndTime = input)}/>
                                                            <Input label='Finish' name='wedEndTime' type='time' onChange={function(e, value) {}} ref={input => (this.wedEndTime = input)}/>
                                                            <Input label='Finish' name='thuEndTime' type='time' onChange={function(e, value) {}} ref={input => (this.thuEndTime = input)}/>
                                                            <Input label='Finish' name='friEndTime' type='time' onChange={function(e, value) {}} ref={input => (this.friEndTime = input)}/>
                                                        </div>
                                                        <div className="col s10 offset-s1">
                                                            <button className="btn col offset-s10" type="submit">Add Schedule</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }}
                            </Mutation>
                            </div>
                        )
                    }
                    
                }}
                </Query>
            </div>
        )
       return null;
    }
}