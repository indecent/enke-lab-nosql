import { createApolloServer } from 'meteor/apollo';
import { makeExecutableSchema } from 'graphql-tools';
import merge from 'lodash/merge';

import UsersResolvers from '../../api/users/resolvers';
import UsersSchema from '../../api/users/User.graphql';
import CoursesResolvers from '../../api/courses/resolvers';
import CoursesSchema from '../../api/courses/Course.graphql';
import StudentsResolvers from '../../api/students/resolvers';
import StudentsSchema from '../../api/students/Student.graphql';
import FacultyResolvers from '../../api/faculty/resolvers';
import FacultySchema from '../../api/faculty/Faculty.graphql';
import CommentResolvers from '../../api/comments/resolvers';
import CommentSchema from '../../api/comments/Comment.graphql';
import ScheduleResolvers from '../../api/schedule/resolvers';
import ScheduleSchema from '../../api/schedule/Schedule.graphql';
import HomeworkResolvers from '../../api/homework/resolvers';
import HomeworkSchema from '../../api/homework/Homework.graphql';

// 0101

const typeDefs = [
  CoursesSchema,
  StudentsSchema,
  FacultySchema,
  UsersSchema,
  CommentSchema,
  ScheduleSchema,
  HomeworkSchema
];

const resolvers = merge(
  CoursesResolvers,
  StudentsResolvers,
  FacultyResolvers,
  UsersResolvers,
  CommentResolvers,
  ScheduleResolvers,
  HomeworkResolvers
);

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

createApolloServer({ schema });
